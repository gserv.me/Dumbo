import os


class Config:
    def __init__(self):
        if not self.bot_token:
            raise ValueError("Must supply the BOT_TOKEN environment variable.")


    @property
    def bot_token(self):
        return os.getenv("BOT_TOKEN", None)
