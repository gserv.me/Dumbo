import logging
from aiohttp import ClientSession

from discord import TextChannel, Message, MessageType
from discord.ext.commands import Bot

log = logging.getLogger(__name__)


class Dumbo:
    def __init__(self, bot: Bot):
        self.bot = bot
        self.webhooks = {}
        self.avatar = b""

    async def on_ready(self):
        log.info(f"Connected; {len(self.bot.guilds)} guilds")

        s = ClientSession()
        r = await s.get("https://cdn.discordapp.com/icons/473546330455801856/3da914d4a7abefb746fe6f374f18006a.png")
        self.avatar = await r.read()
        await s.close()

    async def send(self, message: Message):
        hooks = await message.channel.webhooks()

        if hooks:
            hook = hooks[0]
        else:
            hook = await message.channel.create_webhook(
                name="Dumbo", avatar=self.avatar
            )

        await hook.send(
            message.clean_content,
            username=message.author.display_name,
            avatar_url=message.author.avatar_url_as(format="png")
        )

    async def on_message_delete(self, message: Message):
        if message.type != MessageType.default:
            return  # We don't really want any system messages

        if message.author.bot:
            return  # We don't want any bot messages either

        try:
            await self.send(message)
        except Exception:
            log.exception("Failed to send message")
        else:
            log.exception(f"Message reposted: {message}")


def setup(bot: Bot):
    bot.add_cog(Dumbo(bot))
    log.info("Cog loaded: Dumbo")
